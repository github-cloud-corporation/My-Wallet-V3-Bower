__Blockchain Wallet V3__

#   (2016-08-03)



---

## Features

- **accountinfo:** notifications (#248)
  ([99357a65](https://github.com/blockchain/My-Wallet-V3/commit/99357a65c9203cc7c37e63eabfbc65275d05f417))
- **exporthistory:** add API method for export-history (#249)
  ([bbbd6727](https://github.com/blockchain/My-Wallet-V3/commit/bbbd67270620313ce67e6a5d2759fd7539f18005))


## Chore

- **changelog:** use latest version
  ([5e9b961b](https://github.com/blockchain/My-Wallet-V3/commit/5e9b961b3bae9efcab79382d3d53e68a84ac19bd))
- **release:** v3.21.1
  ([f084ae65](https://github.com/blockchain/My-Wallet-V3/commit/f084ae65d96bb5b6ec19365e7d013c1e80e31c67))


## Pull requests merged

- Merge pull request #246 from blockchain/changelog
  ([3bf4b3e0](https://github.com/blockchain/My-Wallet-V3/commit/3bf4b3e0254c1fcef8d2972f03e52d8e9d538199))



---
<sub><sup>*Generated with [git-changelog](https://github.com/rafinskipg/git-changelog). If you have any problems or suggestions, create an issue.* :) **Thanks** </sub></sup>